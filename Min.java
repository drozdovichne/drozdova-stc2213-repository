
import java.util.Scanner;

class MinDigit{
	public static void main (String[] args) {

		System.out.println("Enter the number!");
		Scanner scanner = new Scanner(System.in);

		int number = scanner.nextInt();
		int minNumber = number;

		while(number != -1){

			if (number < minNumber){
				minNumber = number;
			}

			number = scanner.nextInt();
		}

		System.out.println("Min digit:"+ minNumber);
	}
}
