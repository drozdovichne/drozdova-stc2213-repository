package ru.drozdova.homeworks;

import java.util.Scanner;

public class ReturnIndex {
    public static void main(String[] args) {

        System.out.println("Enter array length:");

        Scanner input = new Scanner(System.in);

        int sizeArray = input.nextInt();

        int[] array = new int[sizeArray];

        System.out.println("Insert array elements:");

        for (int i = 0; i < array.length; i++) {
            array[i] = input.nextInt();
        }

        System.out.println("Enter the number to search him index:");

        int numberSearch = input.nextInt();
        int indexNumberSearch = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == numberSearch) {
                indexNumberSearch = i;
            }
        }

        System.out.println("Index: " + indexNumberSearch);
    }
}
