package src.main.java.ru.drozdova.homeworks;

import java.util.Arrays;
import java.util.Scanner;

public class MovingElements {

    public static void main(String[] args) {

        int[] array = new int[]{34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        int[] arrayNew = new int[array.length];

        System.out.println("Before " + Arrays.toString(array));

        int indexZero = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) {
                arrayNew[indexZero] = array[i];
                indexZero += 1;
            }
        }

        System.out.println("After " + Arrays.toString(arrayNew));
    }
}
