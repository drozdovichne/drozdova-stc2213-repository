import java.util.Scanner;

class OddAndEven {
    public static void main(String[] args) {

        System.out.println("Enter the number!");
        Scanner scanner = new Scanner(System.in);

        int number = scanner.nextInt();
        int amountEvenNumbers = 0;
        int amountOddNumbers = 0;

        while (number != -1) {

            if (number % 2 == 0) {
                amountEvenNumbers = amountEvenNumbers + 1;
            } else {
                amountOddNumbers = amountOddNumbers + 1;
            }
            number = scanner.nextInt();
        }

        System.out.println("amount even numbers:" + amountEvenNumbers);
        System.out.println("amount odd numbers:" + amountOddNumbers);
    }
}
